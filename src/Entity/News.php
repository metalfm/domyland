<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ApiFilter(SearchFilter::class, properties={"author": "exact"})
 * @ApiResource(
 *     attributes={
 *         "order": {"id": "DESC"}
 *     },
 *     collectionOperations={
 *         "get",
 *         "post": {"security": "is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put": {"security": "is_granted('ROLE_SUPER_ADMIN') or object.getAuthor() == user"},
 *     },
 *     normalizationContext={"groups": {"api.read"}},
 *     denormalizationContext={"groups": {"api.write"}}
 * )
 */
class News
{
    public const HABRACUT = '<!--habracut-->';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"api.read"})
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @Groups({"api.read"})
     */
    private $created;

    /**
     * @Groups({"api.read"})
     */
    private $humanCreated;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @Groups({"api.read"})
     */
    private $updated;

    /**
     * @Assert\NotBlank
     * @Assert\Type(type="string")
     * @Assert\Length(min="5", max="30")
     * @ORM\Column(type="string")
     * @Groups({"api.read"})
     * @Groups({"api.write"})
     */
    private $title;

    /**
     * @Assert\NotBlank
     * @Assert\Type(type="string")
     * @Assert\Length(min="20", max="1000")
     * @ORM\Column(type="text")
     * @Groups({"api.read"})
     * @Groups({"api.write"})
     */
    private $text;

    /**
     * @Groups({"api.read"})
     */
    private $previewText;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Groups({"api.read"})
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    public function setHumanCreated(string $humanCreated): self
    {
        $this->humanCreated = $humanCreated;

        return $this;
    }

    public function getHumanCreated(): string
    {
        return $this->humanCreated;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setPreviewText(string $previewText): self
    {
        $this->previewText = $previewText;

        return $this;
    }

    public function getPreviewText(): string
    {
        return $this->previewText;
    }

    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }
}
