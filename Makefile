include .env
export

all:
	echo "Use same command"


start:
	mkdir -p var/cache
	mkdir -p var/log
	mkdir -p vendor
	mkdir -p bin/.phpunit
	mkdir -p web/build
	mkdir -p web/bundles

	cp phpunit.xml.dist phpunit.xml

	docker-compose -f docker-compose.dev.yml up --force-recreate --detach
	mutagen daemon start
	mutagen project start

	# nginx
	mutagen create \
		--default-owner-beta=nginx \
		--scan-mode accelerated \
		--sync-mode one-way-replica \
		--watch-mode-beta no-watch \
		web/ docker://root@domyland_nginx_dev/var/www/html/web

	docker exec -t domyland_nuxt_dev yarn install

	docker exec -u www-data -t domyland_php_dev php composer.phar install -o --no-scripts
	docker exec -u www-data -t domyland_php_dev php bin/phpunit install
	docker exec -u www-data -t domyland_php_dev php bin/console cache:clear
	docker exec -u www-data -t domyland_php_dev php bin/console assets:install web/

	echo "docker exec -t -u www-data domyland_php_dev php composer.phar run-script php-cs && sleep 2 && git add -u" | tee .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

	#make sync


jwt:
	mkdir -p config/jwt
	openssl genpkey -out config/jwt/private-dev.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
	openssl pkey -in config/jwt/private-dev.pem -out config/jwt/public-dev.pem -pubout


stop:
	mutagen project terminate || true
	mutagen terminate -a || true
	mutagen daemon stop || true
	docker-compose -f docker-compose.dev.yml down
	rm -rf web/build/*
	rm -rf web/bundles/*
	rm -rf var/cache/dev/*
	rm -rf var/cache/prod/*
	rm -rf var/cache/test/*
	> var/log/dev.log
	> var/log/test.log
	> var/log/prod.log

	rm -rf assets/.nuxt/*
	rm -rf assets/dist/*


sync:
	rm -rf vendor/*
	docker cp domyland_php_dev:/var/www/html/vendor        ./
	docker cp domyland_php_dev:/var/www/html/var/cache/    ./var/
	docker cp domyland_php_dev:/var/www/html/bin/          ./
	docker cp domyland_php_dev:/var/www/html/composer.json ./composer.json
	docker cp domyland_php_dev:/var/www/html/composer.phar ./composer.phar
	docker cp domyland_php_dev:/var/www/html/composer.lock ./composer.lock
	docker cp domyland_php_dev:/var/www/html/symfony.lock  ./symfony.lock

	rm -rf assets/node_modules/*
	docker cp domyland_nuxt_dev:/assets/node_modules ./assets/


build:
	docker-compose -f docker-compose.dev.yml build \
		--build-arg APP_DEBUG=$$APP_DEBUG \
		--build-arg APP_DEBUG_ADDRESS=docker.for.mac.host.internal \
		--build-arg APP_ENV=$$APP_ENV \
		php

	docker-compose -f docker-compose.dev.yml build \
		--build-arg BASE_URL=$$BASE_URL \
		nuxt

	docker-compose -f docker-compose.dev.yml build \
		--build-arg DOCKER_BUILD_TAG=$$DOCKER_BUILD_TAG \
		nginx


test:
	docker-compose -f docker-compose.dev.yml exec -T php bin/console hautelook:fixtures:load --purge-with-truncate -q
	docker-compose -f docker-compose.dev.yml exec -T -e APP_ENV=$$APP_ENV -e APP_DEBUG=$$APP_DEBUG php bin/phpunit
