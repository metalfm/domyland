<?php declare(strict_types=1);

namespace App\Logger;

class RequestProcessor
{
    private $env;
    private $requestId;

    public function __construct(string $env)
    {
        $this->env = $env;
        $this->requestId = \uniqid('', true);
    }

    public function __invoke(array $record): array
    {
        if (\in_array($this->env, ['dev', 'test'], true)) {
            return $record;
        }

        $record['extra'] = [
            'request_id' => $this->requestId,
        ];

        return $record;
    }
}
