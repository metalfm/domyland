<?php declare(strict_types=1);

namespace App\Common;

use Symfony\Contracts\Translation\TranslatorInterface;

class Formatter
{
    private $locale;
    private $translator;
    private $now;

    public function __construct(string $locale, TranslatorInterface $translator, \DateTimeInterface $now = null)
    {
        $this->locale = $locale;
        $this->translator = $translator;
        $this->now = $now;
    }

    public function formatHumanDateTime(\DateTimeInterface $dateTime): string
    {
        $now = $this->now ?? new \DateTime('now');

        /** @noinspection PhpUnhandledExceptionInspection */
        $diff = (new \DateTime($dateTime->format('Y-m-d')))->diff(new \DateTime($now->format('Y-m-d')));
        if (0 === $diff->invert && 1 === $diff->days) {
            return $this->translator->trans('yesterday', ['date' => $dateTime->format('H:i')]);
        }

        $diff = $dateTime->diff($now);
        if (0 === $diff->invert && $diff->days < 1) {
            return 0 === $diff->h
                ? $this->translator->trans('min', ['min' => $diff->i])
                : $this->translator->trans('hour', ['hour' => $diff->h]);
        }

        $formatter = new \IntlDateFormatter($this->locale, \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM HH:mm');

        return $formatter->format($dateTime);
    }
}
