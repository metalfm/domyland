import { News, User } from '~/types'

export interface State {
  news: News[];
  authorNews: News[];
  newsItem: News | null;
  page: number;
  user: User | null
}
