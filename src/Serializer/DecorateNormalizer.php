<?php declare(strict_types=1);

namespace App\Serializer;

use App\Common\Formatter;
use App\Entity\News;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DecorateNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    private $decorated;
    private $formatter;

    public function __construct(NormalizerInterface $decorated, Formatter $formatter)
    {
        $this->decorated = $decorated;
        $this->formatter = $formatter;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        if ($object instanceof News) {
            $object->setHumanCreated($this->formatter->formatHumanDateTime($object->getCreated()));
            $previewText = \explode(News::HABRACUT, $object->getText());
            $object->setPreviewText($previewText[0] ?? $object->getText());
        }

        return $this->decorated->normalize($object, $format, $context);
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return $this->decorated->denormalize($data, $class, $format, $context);
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        if ($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }
}
