<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateUserCommand extends Command
{
    private $passwordEncoder;
    private $em;
    private $validator;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        parent::__construct();
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
        $this->validator = $validator;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:update-user')
            ->setDescription('Update exist user, press Enter to skip question')
            ->addOption('enabled', null, InputOption::VALUE_NONE, 'enable user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $validationGroups = ['Default'];
        $io = new SymfonyStyle($input, $output);

        $q = new Question('Email user: ');
        $email = $this->getHelper('question')->ask($input, $output, $q);

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        if (null === $user) {
            throw new \RuntimeException(\sprintf('User "%s" not found', $email));
        }

        $q = new Question('Name user: ');
        $name = $this->getHelper('question')->ask($input, $output, $q);
        if (null !== $name) {
            $user->setName($name);
        }

        $q = (new Question('Password user: '))->setHidden(true);
        $password = $this->getHelper('question')->ask($input, $output, $q);

        if (null !== $password) {
            $validationGroups[] = 'password';
            $user
                ->setPlainPassword($password)
                ->setPassword($this->passwordEncoder->encodePassword($user, $password));
        }

        $q = (new Question('Roles user(separate with space): '))
            ->setNormalizer(static function ($roles) {
                return $roles ? \explode(' ', $roles) : null;
            });
        $roles = $this->getHelper('question')->ask($input, $output, $q);
        if (null !== $roles) {
            $user->setRoles($roles);
        }

        $user->setEnabled($input->getOption('enabled'));

        $violations = $this->validator->validate($user, null, $validationGroups);
        if (\count($violations) > 0) {
            $io->error('Error update user, '.((string)$violations));

            return;
        }

        $this->em->flush();
        $io->success(\sprintf('Update user "%s"', $user->getId()));
    }
}
