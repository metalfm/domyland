<?php declare(strict_types=1);

namespace App\Tests\Common;

use App\Common\Formatter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class FormatterTest extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testFormatHumanDate(): void
    {
        $expected = (new \DateTimeImmutable())->setTime(15, 0);
        $now = (new \DateTimeImmutable())->setTime(15, 0);

        $formatter = new Formatter('ru', self::$container->get(TranslatorInterface::class), $now);

        $this->assertEquals($formatter->formatHumanDateTime(new \DateTime('2019-10-11')), '11 октября 00:00');
        $this->assertEquals($formatter->formatHumanDateTime(new \DateTime('2019-10-11 22:30:33')), '11 октября 22:30');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-1 day 23:00')), 'вчера в 23:00');

        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('now')), 'только что');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-1 min')), '1 минуту назад');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-10 min')), '10 минут назад');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-30 min')), '30 минут назад');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-22 min')), '22 минуты назад');

        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-1 hour')), '1 час назад');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-2 hour')), '2 часа назад');
        $this->assertEquals($formatter->formatHumanDateTime($expected->modify('-8 hour')), '8 часов назад');
    }
}
