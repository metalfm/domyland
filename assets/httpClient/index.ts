import axios, { AxiosResponse } from 'axios'
import { CreateNews, News, User } from '~/types'

export const HttpClient = axios.create({
  baseURL: process.env.baseUrl,
  withCredentials: true
})

export const fetchNews = (page: number, authorId?: number): Promise<AxiosResponse<News[]>> => {
  const params: { page: number, author?: number } = { page }
  if (authorId) {
    params.author = authorId
  }

  return HttpClient.get('/news', { params })
}

export const fetchNewsItem = (id: number): Promise<AxiosResponse<News>> => {
  return HttpClient.get(`/news/${id}`)
}

export const fetchMe = (): Promise<AxiosResponse<User>> => {
  return HttpClient.get('/users/me')
}

export const login = (email: string, password: string): Promise<void> => {
  return HttpClient.post('/login', { email, password }, { headers: { 'Content-Type': 'application/json' } })
}

export const editNews = (newsItem: CreateNews): Promise<AxiosResponse<News>> => {
  return HttpClient.put(`/news/${newsItem.id}`, { title: newsItem.title, text: newsItem.text })
}

export const addNews = (newsItem: CreateNews): Promise<AxiosResponse<News>> => {
  return HttpClient.post('/news', { title: newsItem.title, text: newsItem.text })
}
