<?php declare(strict_types=1);

namespace App\Tests\Api;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

trait AuthClientTrait
{
    /**
     * @param array $roles
     * @param array $userOptions
     *
     * @return User
     */
    private function createUser(array $roles, array $userOptions = []): User
    {
        /** @var ContainerInterface $c */
        $c = self::$container;

        $encoder = $c->get(UserPasswordEncoderInterface::class);
        $em = $c->get(EntityManagerInterface::class);

        $user = new User();
        $user
            ->setPlainPassword($userOptions['plainPassword'] ?? 'test')
            ->setPassword($encoder->encodePassword($user, $userOptions['plainPassword'] ?? 'test'))
            ->setEmail($userOptions['email'] ?? 'test@test.ru')
            ->setEnabled($userOptions['enabled'] ?? true)
            ->setRoles($roles)
            ->setName($userOptions['name'] ?? 'userName');

        $em->persist($user);
        $em->flush();

        return $user;
    }

    public function makeAuthRequest(User $user): KernelBrowser
    {
        /** @var KernelBrowser $client */
        $client = self::createClient();
        $content = \GuzzleHttp\json_encode([
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
        ]);
        $client->request('POST', '/api/login', [], [], ['CONTENT_TYPE' => 'application/json'], $content);

        return $client;
    }

    /**
     * @param array $roles
     * @param array $user
     *
     * @return KernelBrowser
     */
    public function createNewAuthenticatedClient(array $roles = ['ROLE_SUPER_ADMIN'], array $user = []): KernelBrowser
    {
        $user = $this->createUser($roles, $user);
        $client = $this->makeAuthRequest($user);

        $response = $client->getResponse();
        if (200 !== $response->getStatusCode()) {
            throw new \RuntimeException($response->getContent(), $response->getStatusCode());
        }
        $data = \GuzzleHttp\json_decode($response->getContent(), true);

        /** @var KernelBrowser $client */
        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', \sprintf('Bearer %s', $data['token']));

        return $client;
    }

    public function createAuthenticatedClient(User $user): KernelBrowser
    {
        $client = $this->makeAuthRequest($user);

        $response = $client->getResponse();
        if (200 !== $response->getStatusCode()) {
            throw new \RuntimeException($response->getContent(), $response->getStatusCode());
        }
        $data = \GuzzleHttp\json_decode($response->getContent(), true);

        /** @var KernelBrowser $client */
        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', \sprintf('Bearer %s', $data['token']));

        return $client;
    }
}
