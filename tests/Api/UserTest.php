<?php declare(strict_types=1);

namespace App\Tests\Api;

use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserTest extends WebTestCase
{
    use MatchesSnapshots;
    use AuthClientTrait;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testGetMe(): void
    {
        $em = self::$container->get(EntityManagerInterface::class);
        /** @var News $new */
        $new = $em->getRepository(News::class)->find(2);
        $user = $new->getAuthor();

        $client = $this->createAuthenticatedClient($user);
        $client->request('GET', '/api/users/me.json');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot(\GuzzleHttp\json_decode($response->getContent(), true));
    }
}
