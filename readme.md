## Domyland
Чудесные новости. В качестве бекэнда выбран Symfony — имхо лучшее что есть в мире php.
В качестве api используется `api-platform`, умеет парсить метаданные сущностей и строить админку на основе `Swagger-ui`, валидация, security прослойка, гибкая кастомизация и генерация объектов из open api спецификации.
Логи в `dev` и `test` окружении пишутся в `bunyan` формате, удобно смотреть в консоли, можно фильтровать по разным уровням лога. На проде логи пишутся в формате `elasticsearch` и дополняются `request_id` для удобной фильтрации.
В качестве аутентификации используется jwt, в отличие от других способов легко масштабировать приложение на много серверов.
Для локальной разработки выбран `docker` в связке с `mutagen` для быстрой синхронизации файлов на osx, сборка и запуск контейнеров через старый добрый `Makefile`.
Папка `.idea` осознано оказалась в системе контроля версий для шаринга настроек шторма и окружения. Настроен php, тесты, интеграция с докером, код-стайл — можно быстро вводить в проект новых людей. 


## Install, build and run — OSx only
- `cp .env.dist .env`
- `brew install havoc-io/mutagen/mutagen`
- add alias to ~/.bash_profile
```
alias dc="docker-compose"
alias dc-dev='docker-compose  -f `pwd`/docker-compose.dev.yml '
alias dc-prod='docker-compose -f `pwd`/docker-compose.prod.yml '
alias dc-exterminatus='docker stop $(docker ps -a -q) > /dev/null 2>&1; docker rm $(docker ps -a -q) > /dev/null 2>&1; docker rmi $(docker images -f "dangling=true" -q) > /dev/null 2>&1; echo 🔥'
```
- `make:jwt`, pass phrase можно посмотреть в `.env.dist`
- install [docker](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
- `make build`
- `make start`
- `dc-dev exec php bin/console doctrine:migrations:migrate` накатить миграции
- `dc-dev exec php bin/console app:create-user admin@domyland.ru Евгений admin --role=ROLE_SUPER_ADMIN --enabled` создать нового админа
- api backend — [localhost](http://localhost/api)
- frontend — [localhost](http://localhost:3000/news)
- `make stop`


## Аутентификация
Пока получать jwt токен можно через cli
```
curl -X POST -H "Content-Type: application/json" "http://localhost/api/login" -d '{"email":"admin@domyland.ru","password":"admin"}'
```
Полученый токен можно использовать в админке, достаточно нажать Authorize, и написать `Bearer <token>`


## Прод
В идеале kubernetes, но можно и с помощью compose, для это нужно поменять `APP_ENV=prod`, `APP_DEBUG=0`, `BASE_URL=/api`, сделать билд `make build`, запустить контейнеры `dc-dev up nginx php mysql` 
и посмотреть [фронт](http://localhost/news) и [апи](http://localhost/api).


## Тесты
`make test` осторожно, данная команда удаляет все данные из базы и заполняет их фикстурами.


## Полезные cli команды

`tail -f var/log/dev.log | bunyan -L -l info` посмотреть логи `dev` окружения и показывать всё что больше или равно уровню логгирования `info`

`dc-dev exec php sh` подключиться по ssh к php контейнеру, все команды описанные ниже выполняются внутри контейнера

`bin/console app:create-user <email> <userName> <plainPassword> --role=ROLE_ADMIN --enabled` создать нового пользователя

`bin/console app:update-user --enabled` обновить уже существующего пользователя

`php composer.phar run-script php-cs` откодстайлить весь проект


## Отладка в веб
- установить [xdebug helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=ru)
- активировать xdebug helper
- включить PHP Debug Connections ![](https://www.drupal.org/files/listen.png)
- ставим break point


## Отладка в cli
- подключаемся по ssh в php контейнер и экспортим переменные окружения
```
dc-dev exec -u www-data php sh
export XDEBUG_CONFIG="idekey=PHPSTORM"
export PHP_IDE_CONFIG="serverName=domyland"
```
- включить PHP Debug Connections
- ставим break point
- запускаем отлаживаемый cli скрипт


## Что можно сделать лучше?

1. Добавить http кеш. Допустим наше api очень популярно и много миллионов пользователей каждый день читают новости на нашем ресурсе, создавая существенную нагрузку на сервер. 
Можно отдавать данные из кеша Varnish(Nginx) используя схему с обратным прокси сервером, причём если кеша нет(или протух), то только первый запрос прилетает на бекенд прогревая кеш, а остальные ждут и получают ответ из кеша.

2. Отпрофилировать все запросы и расставить индексы, оптимизировать SearchFilter для новостей и избавиться от лишнего подзапроса.

3. Закрыть документацию по `/api` аутентификкацией и авторизацией, генерировать схему и доступные действия в зависимости от роли юзера.

4. Добавить механизм обновления jwt токенов, сейчас токены живут ровно 1 час и по истечению срока жизни юзеру нужно снова логиниться.

5. Заменить текущие сообщения валидации на более дружелюбные используя `translator`. 

6. Провести ревизию Docker образов, php образ можно сделать меньше используя `multi-stage builds` для разных типов окружения(dev, test, prod). Добавить `filebeat` для траспорта логов в `elasticsearch` на проде. 
