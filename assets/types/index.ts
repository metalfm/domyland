export * from './state'

export interface CreateNews {
  id?: number;
  title: string;
  text: string;
}

export interface News {
  id: number;
  humanCreated: string;
  title: string;
  text: string;
  previewText: string;
  author: User;
}

export interface User {
  id: number;
  name: string;
}

export interface Violation {
  propertyPath: string;
  message: string;
}

export interface ViolationError {
  title: string;
  detail: string;
  violations: Violation[];
}
