<?php declare(strict_types=1);

namespace App\Tests\Api;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthTest extends WebTestCase
{
    use AuthClientTrait;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testBadRequest(): void
    {
        $client = self::createClient();
        $client->request('POST', '/api/login', [], [], ['CONTENT_TYPE' => 'application/json']);

        $response = $client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertStringContainsString('Bad Request', $response->getContent());
    }

    public function testNotFoundUser(): void
    {
        $client = self::createClient();
        $content = \GuzzleHttp\json_encode([
            'email' => 'notfound@mail.ru',
            'password' => 'test',
        ]);
        $client->request('POST', '/api/login', [], [], ['CONTENT_TYPE' => 'application/json'], $content);

        $response = $client->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
        $this->assertStringContainsString('Bad credentials', $response->getContent());
    }

    public function testDisabledUser(): void
    {
        try {
            $this->createNewAuthenticatedClient(['ROLE_SUPER_ADMIN'], ['enabled' => false]);
        } catch (\RuntimeException $e) {
            $this->assertEquals(401, $e->getCode());
            $this->assertEquals('{"code":401,"message":"User account is disabled."}', $e->getMessage());
        }
    }

    public function testSuccessAuth(): void
    {
        $client = $this->createNewAuthenticatedClient();
        $token = $client->getServerParameter('HTTP_Authorization', null);

        $this->assertIsString($token);
        $this->assertNotEmpty($token);
    }

    public function testSuccessAuthCookie(): void
    {
        $em = self::$container->get(EntityManagerInterface::class);
        /** @var User $user */
        $user = $em->getRepository(User::class)->find(1);
        $client = $this->makeAuthRequest($user);

        $cookies = $client->getResponse()->headers->get('set-cookie');
        $this->assertStringContainsString('BEARER=', $cookies);
    }
}
