<?php declare(strict_types=1);

namespace App\EventSubscriber;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthSubscriber implements EventSubscriberInterface
{
    private $ttl;
    private $requestStack;

    public function __construct(int $ttl, RequestStack $requestStack)
    {
        $this->ttl = $ttl;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::AUTHENTICATION_SUCCESS => 'onAuthSuccess',
        ];
    }

    public function onAuthSuccess(AuthenticationSuccessEvent $event): void
    {
        $bearerCookie = $this->requestStack->getCurrentRequest()->cookies->get('BEARER');
        if (null === $bearerCookie) {
            $response = $event->getResponse();
            $cookie = new Cookie('BEARER', $event->getData()['token'], new \DateTime("+{$this->ttl} seconds"), null, null, false, true, false, null);
            $response->headers->setCookie($cookie);
            $response->sendHeaders();
        }
    }
}
