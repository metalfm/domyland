import { Context } from '@nuxt/types'
import { FETCH_ME } from '~/store'

export default async(ctx: Context) => {
  try {
    await ctx.store.dispatch(FETCH_ME)
  } catch (e) {
  }
}
