<?php declare(strict_types=1);

namespace App\Tests\EventSubscriber;

use App\EventSubscriber\RequestEventSubscriber;
use PHPUnit\Framework\TestCase;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

class RequestEventSubscriberTest extends TestCase
{
    use MatchesSnapshots;

    /**
     * @var RequestEventSubscriber
     */
    private $subscriber;

    protected function setUp(): void
    {
        $this->subscriber = new RequestEventSubscriber();
    }

    public function testSkipDecodeRequest(): void
    {
        $request = new Request();
        $request->headers->replace(['CONTENT_TYPE' => null]);
        $request->request->replace(['request' => '{"key":"value"}']);

        $controllerEvent = $this->createMock(ControllerEvent::class);
        $controllerEvent->method('getRequest')->willReturn($request);

        $this->subscriber->convertJsonRequest($controllerEvent);
        $this->assertMatchesSnapshot($request->request->all());
    }

    public function testSuccessDecodeJsonRequest(): void
    {
        $request = new Request([], [], [], [], [], [], '{"key":"value"}');
        $request->headers->replace(['CONTENT_TYPE' => 'application/json']);

        $controllerEvent = $this->createMock(ControllerEvent::class);
        $controllerEvent->method('getRequest')->willReturn($request);

        $this->subscriber->convertJsonRequest($controllerEvent);
        $this->assertMatchesSnapshot($request->request->all());
    }

    public function testExceptionDecodeJsonRequest(): void
    {
        $request = new Request([], [], [], [], [], [], 'invalidJsonContent');
        $request->headers->replace(['CONTENT_TYPE' => 'application/json']);

        $controllerEvent = $this->createMock(ControllerEvent::class);
        $controllerEvent->method('getRequest')->willReturn($request);

        $this->expectExceptionMessage('Json decode error');
        $this->subscriber->convertJsonRequest($controllerEvent);
    }
}
