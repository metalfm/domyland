<?php declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\News;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

final class NewsDataPersister implements ContextAwareDataPersisterInterface
{
    private $security;
    private $em;

    public function __construct(Security $security, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->em = $em;
    }

    public function supports($news, array $context = []): bool
    {
        return $news instanceof News;
    }

    /**
     * @param News  $news
     * @param array $context
     *
     * @return object|void
     */
    public function persist($news, array $context = [])
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if ('post' === ($context['collection_operation_name'] ?? null)) {
            $news->setAuthor($user);
            $this->em->persist($news);
        }

        $this->em->flush();

        return $news;
    }

    public function remove($news, array $context = []): void
    {
    }
}
