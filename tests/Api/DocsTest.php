<?php declare(strict_types=1);

namespace App\Tests\Api;

use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DocsTest extends WebTestCase
{
    use MatchesSnapshots;

    public function testSuccess(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/docs.json');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot(\GuzzleHttp\json_decode($response->getContent(), true));
    }
}
