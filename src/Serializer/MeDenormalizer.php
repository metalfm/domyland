<?php declare(strict_types=1);

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class MeDenormalizer implements DenormalizerInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if (null !== $user) {
            return $user->getId();
        }

        return $data;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return User::ME === $data;
    }
}
