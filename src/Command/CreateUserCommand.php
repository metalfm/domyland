<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserCommand extends Command
{
    private $passwordEncoder;
    private $em;
    private $validator;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        parent::__construct();
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
        $this->validator = $validator;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:create-user')
            ->setDescription('Create new user')
            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED)
            ->addOption('role', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'role of user', ['ROLE_USER'])
            ->addOption('enabled', null, InputOption::VALUE_NONE, 'enable user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $user = (new User())
            ->setEmail($input->getArgument('email'))
            ->setName($input->getArgument('name'))
            ->setPlainPassword($input->getArgument('password'))
            ->setRoles($input->getOption('role'))
            ->setEnabled($input->getOption('enabled'));

        $hashedPassword = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($hashedPassword);

        $violations = $this->validator->validate($user, null, ['Default', 'password']);
        if (\count($violations) > 0) {
            $io->error('Error create user, '.((string)$violations));

            return;
        }

        $this->em->persist($user);
        $this->em->flush();

        $io->success(\sprintf('Create user "%s"', $user->getId()));
    }
}
