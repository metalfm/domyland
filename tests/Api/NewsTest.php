<?php declare(strict_types=1);

namespace App\Tests\Api;

use App\Entity\News;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Security;

class NewsTest extends WebTestCase
{
    use MatchesSnapshots;
    use AuthClientTrait;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testGetCollection(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/news.json');

        $response = $client->getResponse();
        $news = \GuzzleHttp\json_decode($response->getContent(), true);
        foreach ($news as &$new) {
            unset($new['created'], $new['updated'], $new['humanCreated']);
        }
        unset($new);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot($news);
    }

    public function testGetItem(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/news/1.json');
        $response = $client->getResponse();

        $new = \GuzzleHttp\json_decode($response->getContent(), true);
        unset($new['created'], $new['updated'], $new['humanCreated']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot($new);
    }

    public function testAuthorFilter(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/news.json?author=1');

        $response = $client->getResponse();
        $news = \GuzzleHttp\json_decode($response->getContent(), true);
        foreach ($news as &$new) {
            unset($new['created'], $new['updated'], $new['humanCreated']);
        }
        unset($new);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot($news);
    }

    public function testCreateUnAuthenticate(): void
    {
        $client = self::createClient();
        $client->request('POST', '/api/news.json');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testPostUnAuthorized(): void
    {
        $client = $this->createNewAuthenticatedClient(['ROLE_USER']);
        $client->request('POST', '/api/news.json', [], [], ['CONTENT_TYPE' => 'application/json'], '{}');

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function provideInvalidCreate(): \Generator
    {
        yield [[]];
        yield [['title' => '', 'text' => '']];
        yield [['title' => 'validTitle', 'text' => 'smallText']];
    }

    /**
     * @param array $newsData
     *
     * @dataProvider provideInvalidCreate
     */
    public function testInvalidPost(array $newsData): void
    {
        $content = \GuzzleHttp\json_encode($newsData);
        $client = $this->createNewAuthenticatedClient();
        $client->request('POST', '/api/news.json', [], [], ['CONTENT_TYPE' => 'application/json'], $content);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertMatchesSnapshot(\GuzzleHttp\json_decode($client->getResponse()->getContent(), true));
    }

    public function testSuccessPost(): void
    {
        $content = \GuzzleHttp\json_encode([
            'title' => 'validTitle',
            'text' => 'validLongTextValidLongText',
        ]);
        $client = $this->createNewAuthenticatedClient();
        $client->request('POST', '/api/news.json', [], [], ['CONTENT_TYPE' => 'application/json'], $content);

        $response = $client->getResponse();
        $new = \GuzzleHttp\json_decode($response->getContent(), true);

        /** @var User $user */
        $user = self::$container->get(Security::class)->getUser();
        $this->assertEquals($new['author']['id'], $user->getId());

        unset($new['id'], $new['created'], $new['updated'], $new['humanCreated'], $new['author']['id']);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertMatchesSnapshot($new);
    }

    public function testPutUnAuthorized(): void
    {
        $client = $this->createNewAuthenticatedClient(['ROLE_USER']);
        $client->request('PUT', '/api/news/1.json', [], [], ['CONTENT_TYPE' => 'application/json'], '{}');

        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testAuthorPut(): void
    {
        $em = self::$container->get(EntityManagerInterface::class);
        /** @var News $new */
        $new = $em->getRepository(News::class)->find(1);
        $user = $new->getAuthor();

        $client = $this->createAuthenticatedClient($user);
        $content = \GuzzleHttp\json_encode([
            'title' => 'validTitle',
            'text' => 'validLongTextValidLongText',
        ]);

        $client->request('PUT', '/api/news/1.json', [], [], ['CONTENT_TYPE' => 'application/json'], $content);
        $response = $client->getResponse();

        $new = \GuzzleHttp\json_decode($response->getContent(), true);
        unset($new['id'], $new['created'], $new['humanCreated'], $new['updated'], $new['author']['id']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertMatchesSnapshot($new);

        /** @var News $new */
        $new = $em->getRepository(News::class)->find(1);
        $this->assertMatchesSnapshot([
            'title' => $new->getTitle(),
            'text' => $new->getText(),
        ]);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function testSameAuthorDisablePut(): void
    {
        $em = self::$container->get(EntityManagerInterface::class);
        /** @var News $new */
        $new = $em->getRepository(News::class)->find(1);
        $user = $new->getAuthor();

        /** @var News $anotherNew */
        $anotherNew = $em
            ->createQueryBuilder()
            ->select('n')
            ->from(News::class, 'n')
            ->where('n.author <> :author_id')
            ->setParameter('author_id', $user->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $client = $this->createAuthenticatedClient($user);
        $client->request('PUT', \sprintf('/api/news/%d.json', $anotherNew->getId()), [], [], ['CONTENT_TYPE' => 'application/json'], '{}');
        $response = $client->getResponse();

        $this->assertEquals(403, $response->getStatusCode());
        $this->assertStringContainsString('Access Denied.', $response->getContent());
    }
}
