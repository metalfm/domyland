import { ActionContext, ActionTree, MutationTree } from 'vuex'
import { CreateNews, News, State, User } from '~/types'
import { fetchMe, fetchNews, fetchNewsItem, login, editNews, addNews } from '~/httpClient'

export const state = (): State => ({
  news: [],
  page: 1,
  newsItem: null,
  authorNews: [],
  user: null
})

export const SET_NEWS = 'set_news'
export const SET_AUTHOR_NEWS = 'set_author_news'
export const ADD_NEWS_ITEM = 'add_news_item'
export const SET_USER = 'set_user'

export const mutations: MutationTree<State> = {
  [SET_NEWS]: (
    state: State,
    { news, page }: { news: News[]; page: number }
  ) => {
    state.news = news
    state.page = page
  },

  [ADD_NEWS_ITEM]: (state: State, newsItem: News) => {
    state.newsItem = newsItem
  },

  [SET_AUTHOR_NEWS]: (state: State, news: News[]) => {
    state.authorNews = news
  },

  [SET_USER]: (state: State, user?: User) => {
    state.user = user || null
  }
}

export const ADD_OR_EDIT_NEWS = 'add_or_edit_news'
export const FETCH_NEWS = 'fetch_news'
export const FETCH_NEWS_ITEM = 'fetch_news_item'
export const FETCH_ME = 'fetch_me'
export const LOGIN = 'login'

export const actions: ActionTree<State, State> = {
  [FETCH_NEWS]: async(
    context: ActionContext<State, State>,
    authorId?: number
  ) => {
    const { data } = await fetchNews(context.state.page, authorId)
    if (authorId) {
      context.commit(SET_AUTHOR_NEWS, data)
    } else {
      context.commit(SET_NEWS, { news: data, page: context.state.page })
    }
  },

  [FETCH_NEWS_ITEM]: async(
    context: ActionContext<State, State>,
    id: number
  ) => {
    const items = context.state.news.filter(newsItem => newsItem.id === id)
    const newsItem = items[0] || null

    if (!newsItem) {
      const { data } = await fetchNewsItem(id)
      context.commit(ADD_NEWS_ITEM, data)
    } else {
      context.commit(ADD_NEWS_ITEM, newsItem)
    }
  },

  [FETCH_ME]: async(context: ActionContext<State, State>) => {
    if (!context.state.user) {
      try {
        const { data } = await fetchMe()
        context.commit(SET_USER, data)
      } catch (e) {
        context.commit(SET_USER, null)
      }
    }
  },

  [LOGIN]: async(
    context: ActionContext<State, State>,
    { email, password }: { email: string, password: string }
  ) => {
    try {
      await login(email, password)
      await context.dispatch(FETCH_ME)
    } catch (e) {
      throw e
    }
  },

  [ADD_OR_EDIT_NEWS]: async(ctx: ActionContext<State, State>, newsItem: CreateNews) => {
    try {
      if (newsItem.id) {
        const { data } = await editNews(newsItem)
        ctx.commit(ADD_NEWS_ITEM, data)
      } else {
        const { data } = await addNews(newsItem)
        ctx.commit(ADD_NEWS_ITEM, data)
      }
    } catch (e) {
      throw e
    }
  }
}

export const getters = {
  news: (state: State): News[] => state.news,
  authorNews: (state: State): News[] => state.authorNews,
  newsItem: (state: State): News | null => state.newsItem
}
